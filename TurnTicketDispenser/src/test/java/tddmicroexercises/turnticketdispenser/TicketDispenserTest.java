package tddmicroexercises.turnticketdispenser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class TicketDispenserTest {

    private TicketDispenser ticketDispenser;

    @BeforeEach
    void setUp() {
        TurnNumberSequence turnNumberSequence = new TurnNumberSequence();
        ticketDispenser = new TicketDispenser(turnNumberSequence);
    }

    @Test
    void getTurnTicket() {
        for (int i = 0; i < 10; i++) {
            assertEquals(i, ticketDispenser.getTurnTicket().getTurnNumber());
        }
    }
}