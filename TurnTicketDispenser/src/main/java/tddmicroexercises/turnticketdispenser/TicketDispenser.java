package tddmicroexercises.turnticketdispenser;

public class TicketDispenser {

    private final NumberGenerator numberGenerator;

    public TicketDispenser(NumberGenerator numberGenerator) {
        this.numberGenerator = numberGenerator;
    }

    public TurnTicket getTurnTicket() {

        int newTurnNumber = numberGenerator.getNextTurnNumber();

        return new TurnTicket(newTurnNumber);
    }
}
