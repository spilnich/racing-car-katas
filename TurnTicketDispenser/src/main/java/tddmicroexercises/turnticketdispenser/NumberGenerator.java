package tddmicroexercises.turnticketdispenser;

public interface NumberGenerator {

    int getNextTurnNumber();
}
