package tddmicroexercises.turnticketdispenser;

public class TurnNumberSequence implements NumberGenerator {
    private int turnNumber = 0;

    @Override
    public int getNextTurnNumber() {
        return turnNumber++;
    }
}
