package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class HtmlTextConverter {
    private final String fullFilenameWithPath;
    private final Escaper escaper;

    public HtmlTextConverter(Escaper escaper, String fullFilenameWithPath) {
        this.escaper = escaper;
        this.fullFilenameWithPath = fullFilenameWithPath;
    }

    public String convertToHtml() throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fullFilenameWithPath))) {
            return reader.lines()
                    .map(escaper::escape)
                    .collect(Collectors.joining());
        }
    }

    public String getFilename() {
        return this.fullFilenameWithPath;
    }
}
