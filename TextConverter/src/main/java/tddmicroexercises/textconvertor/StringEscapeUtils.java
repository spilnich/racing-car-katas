package tddmicroexercises.textconvertor;

class StringEscapeUtils implements Escaper {

    @Override
    public String escape(String line) {
        String output = line;
        output = output.replace("&", "&amp;");
        output = output.replace("<", "&lt;");
        output = output.replace(">", "&gt;");
        output = output.replace("\"", "&quot;");
        output = output.replace("'", "&quot;");
        output = output + "<br />";
        return output;
    }
}
