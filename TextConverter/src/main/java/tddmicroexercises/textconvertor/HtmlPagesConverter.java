package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlPagesConverter {

    private final String filename;
    private final List<Integer> breaks = new ArrayList<>();
    private final Escaper escaper;

    public HtmlPagesConverter(Escaper escaper, String filename) throws IOException {
        this.escaper = escaper;
        this.filename = filename;
        this.breaks.add(0);
        getCharsCount();
    }

    private void getCharsCount() throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
            reader.lines()
                    .takeWhile(line -> !line.contains("PAGE_BREAK"))
                    .map(line -> line.length() + 1)
                    .forEach(breaks::add);
        }
    }

    public String getHtmlPage(int page) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
            return reader.lines()
                    .skip(breaks.get(page))
                    .takeWhile(line -> !line.contains("PAGE_BREAK"))
                    .map(escaper::escape)
                    .collect(Collectors.joining());
        }
    }

    public String getFilename() {
        return this.filename;
    }
}
