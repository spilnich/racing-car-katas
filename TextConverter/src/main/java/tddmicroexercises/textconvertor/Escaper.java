package tddmicroexercises.textconvertor;

public interface Escaper {

    String escape(String line);
}
