package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlPagesConverterTest {

    public static final String SRC_TEST_JAVA_RESOURCES_TEST_FILE_TXT = "src/test/java/resources/testFile.txt";

    @Test
    void foo() throws IOException {
        HtmlPagesConverter converter = new HtmlPagesConverter(new StringEscapeUtils(),
                SRC_TEST_JAVA_RESOURCES_TEST_FILE_TXT);
        assertEquals("src/test/java/resources/testFile.txt", converter.getFilename());
    }

    @Test
    void testGetHtmlPage() throws IOException {
        HtmlPagesConverter htmlPagesConverter = new HtmlPagesConverter(new StringEscapeUtils(),
                SRC_TEST_JAVA_RESOURCES_TEST_FILE_TXT);

        assertEquals("&lt;SPD-University&gt;<br />", htmlPagesConverter.getHtmlPage(0));
    }
}
