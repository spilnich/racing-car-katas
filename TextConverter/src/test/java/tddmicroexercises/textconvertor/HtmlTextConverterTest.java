package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlTextConverterTest {

    public static final String SRC_TEST_JAVA_RESOURCES_TEST_FILE_TXT = "src/test/java/resources/testFile.txt";

    @Test
    void foo() {
        HtmlTextConverter converter = new HtmlTextConverter(new StringEscapeUtils(), "foo");
        assertEquals("foo", converter.getFilename());
    }

    @Test
    void testHtmlTextConverter() throws IOException {
        HtmlTextConverter converter = new HtmlTextConverter(new StringEscapeUtils(),
                SRC_TEST_JAVA_RESOURCES_TEST_FILE_TXT);

        assertEquals("&lt;SPD-University&gt;<br />PAGE_BREAK<br />", converter.convertToHtml());
    }
}
