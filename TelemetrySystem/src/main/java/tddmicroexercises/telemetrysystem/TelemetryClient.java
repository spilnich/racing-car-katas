package tddmicroexercises.telemetrysystem;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TelemetryClient implements Connection {
    public static final String DIAGNOSTIC_MESSAGE = "AT#UD";

    private boolean onlineStatus;
    private String diagnosticMessageResult = "";

    private final Random connectionEventsSimulator = new Random(42);

    @Override
    public boolean getOnlineStatus() {
        return onlineStatus;
    }

    @Override
    public void connect(String telemetryServerConnectionString) {
        if (telemetryServerConnectionString == null || "".equals(telemetryServerConnectionString)) {
            throw new IllegalArgumentException();
        }

        // simulate the operation on a real modem

        onlineStatus = connectionEventsSimulator.nextInt(10) <= 8;
    }

    @Override
    public void disconnect() {
        onlineStatus = false;
    }

    @Override
    public void send(String message) {
        if (message == null || "".equals(message)) {
            throw new IllegalArgumentException();
        }

        if (message.equals(DIAGNOSTIC_MESSAGE)) {
            // simulate a status report
            diagnosticMessageResult =
                    "LAST TX rate................ 100 MBPS\r\n" + "HIGHEST TX rate............. 100 MBPS\r\n"
                            + "LAST RX rate................ 100 MBPS\r\n" + "HIGHEST RX rate............. 100 MBPS\r\n"
                            + "BIT RATE.................... 100000000\r\n" + "WORD LEN.................... 16\r\n"
                            + "WORD/FRAME.................. 511\r\n" + "BITS/FRAME.................. 8192\r\n"
                            + "MODULATION TYPE............. PCM/FM\r\n" + "TX Digital Los.............. 0.75\r\n"
                            + "RX Digital Los.............. 0.10\r\n" + "BEP Test.................... -5\r\n"
                            + "Local Rtrn Count............ 00\r\n" + "Remote Rtrn Count........... 00";
        }

        // here should go the real Send operation (not needed for this exercise)
    }

    @Override
    public String receive() {
        if (diagnosticMessageResult == null || "".equals(diagnosticMessageResult)) {
            return IntStream.range(0, connectionEventsSimulator.nextInt(50) + 60)
                    .map(i -> i = connectionEventsSimulator.nextInt(40) + 86)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining());
        } else {
            String message = diagnosticMessageResult;
            diagnosticMessageResult = "";
            return message;
        }
    }
}

