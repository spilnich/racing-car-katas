package tddmicroexercises.telemetrysystem;

public interface Connection {

    void connect(String telemetryServerConnectionString);

    void disconnect();

    void send(String message);

    String receive();

    boolean getOnlineStatus();
}
