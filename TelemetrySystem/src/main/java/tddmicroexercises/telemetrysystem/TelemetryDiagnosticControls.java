package tddmicroexercises.telemetrysystem;

import java.net.ConnectException;

public class TelemetryDiagnosticControls {

    private final Connection connection;
    private String diagnosticInfo = "";

    public TelemetryDiagnosticControls(Connection connection) {
        this.connection = connection;
    }

    public String getDiagnosticInfo() {
        return diagnosticInfo;
    }

    public void checkTransmission() throws ConnectException {
        diagnosticInfo = "";

        connection.disconnect();

        int retryLeft = 3;
        while (!connection.getOnlineStatus() && retryLeft > 0) {
            String diagnosticChannelConnectionString = "*111#";
            connection.connect(diagnosticChannelConnectionString);
            retryLeft -= 1;
        }

        if (!connection.getOnlineStatus()) {
            throw new ConnectException("Unable to connect.");
        }

        connection.send(TelemetryClient.DIAGNOSTIC_MESSAGE);
        diagnosticInfo = connection.receive();
    }
}
