package tddmicroexercises.tirepressuremonitoringsystem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestAlarm {
    private Alarm alarm;
    private double pressure;

    @BeforeEach
    void setUp() {
        Sensor sensor = new Sensor();
        alarm = new Alarm(sensor);
        pressure = sensor.popNextPressurePsiValue();
    }

    @Test
    void isAlarmOff() {
        assertFalse(alarm.isAlarmOn());
    }

    @Test
    void checkAlarmLowPressure() {
        if (pressure < 16.0) {
            alarm.check();
            assertTrue(alarm.alarmOn);
        }
    }

    @Test
    void checkAlarmHighPressure() {
        if (pressure > 21.0) {
            alarm.check();
            assertTrue(alarm.alarmOn);
        }
    }

    @Test
    void checkAlarmOk() {
        if (pressure >= 17 && pressure <= 21) {
            alarm.check();
            assertFalse(alarm.alarmOn);
        }
    }
}
