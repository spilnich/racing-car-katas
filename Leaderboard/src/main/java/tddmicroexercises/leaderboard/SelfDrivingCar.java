package tddmicroexercises.leaderboard;

public class SelfDrivingCar implements DriverId {

    private String algorithmVersion;
    private final String country;

    public SelfDrivingCar(String algorithmVersion, String country) {
        this.algorithmVersion = algorithmVersion;
        this.country = country;
    }

    public void setAlgorithmVersion(String algorithmVersion) {
        this.algorithmVersion = algorithmVersion;
    }

    @Override
    public String getName() {
        return "Self Driving Car - " + country + " (" + algorithmVersion + ")";
    }
}
