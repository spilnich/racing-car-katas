package tddmicroexercises.leaderboard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Leaderboard {

    private final List<Race> races;

    public Leaderboard(Race... races) {
        this.races = Arrays.asList(races);
    }

    public Map<String, Integer> driverResults() {
        return races.stream().collect(
                HashMap::new, (map, race) -> race.getResults()
                        .forEach(driverId -> putResults(
                                map, race.getDriverName(driverId), race.getPoints(driverId))),
                Map::putAll);
    }

    private void putResults(Map<String, Integer> results, String driverName, int points) {
        if (results.containsKey(driverName)) {
            results.put(driverName, results.get(driverName) + points);
        } else {
            results.put(driverName, points);
        }
    }

    public List<String> driverRankings() {
        return driverResults().entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
