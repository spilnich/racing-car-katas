package tddmicroexercises.leaderboard;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Race {

    private static final Integer[] POINTS = new Integer[]{25, 18, 15};

    private final String name;
    private final List<DriverId> results;
    private final Map<DriverId, String> driverNames;

    public Race(String name, DriverId... drivers) {
        this.name = name;
        this.results = Arrays.asList(drivers);
        driverNames = driverNamesInit();
    }

    private Map<DriverId, String> driverNamesInit() {
        return results.stream()
                .collect(Collectors.toMap(driverId -> driverId, DriverId::getName));
    }

    public int position(DriverId driver) {
        return this.results.indexOf(driver);
    }

    public int getPoints(DriverId driver) {
        return Race.POINTS[position(driver)];
    }

    public List<DriverId> getResults() {
        return results;
    }

    public String getDriverName(DriverId driver) {
        return this.driverNames.get(driver);
    }

    @Override
    public String toString() {
        return name;
    }
}
